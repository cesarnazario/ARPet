using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ARPet : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _text;
    [SerializeField]
    private AudioSource _audioEat;
    [SerializeField]
    private AudioSource _audioPet;
    [SerializeField]
    private AudioSource _audioFloat;
    private NavMeshAgent _navAgent = null;
    private Animator _anim;
    private VerticalLayoutGroup _messages;
    private Vector3 _destination;
    private Camera _arCamera;
    private bool _isGettingPizza = false;
    public bool IsAwake = true;

    private void Awake()
    {
        _arCamera = FindObjectOfType<Camera>();
        _messages = FindObjectOfType<VerticalLayoutGroup>();
        _navAgent = GetComponent<NavMeshAgent>();
        _anim = GetComponentInChildren<Animator>();
    }

    private void FixedUpdate()
    {
        _anim.SetFloat("MoveZ", _navAgent.velocity.magnitude);
        if (!_navAgent.isStopped)
        {
            _navAgent.SetDestination(_destination);
            PlayFootsteps();
        }
        if (_isGettingPizza && _navAgent.remainingDistance == 0)
        {
            _audioEat.Play();
            _anim.SetTrigger("Bite Attack");
            var pizza = FindObjectOfType<Pizza>();
            Destroy(pizza.gameObject);
            _isGettingPizza = false;
            InteractionManager.IsPizzaActive = false;
        }
    }

    public void Pet()
    {
        _anim.SetTrigger("Cast Spell");
        _navAgent.isStopped = false;
        _audioPet.Play();
    }

    public void Sleep()
    {
        _anim.SetBool("Sleep", true);
        _navAgent.isStopped = true;
        IsAwake = true;
    }
    public void WakeUp()
    {
        _anim.SetBool("Sleep", false);
        IsAwake = false;
        _audioPet.Play();
        _navAgent.isStopped = false;
    }

    public void SetNavMeshDestination(Vector3 pos)
    {
        _destination = pos;
        _isGettingPizza = true;
        //_navAgent.SetDestination(pos);
    }

    private void PlayFootsteps()
    {
        _audioFloat.volume = Random.Range(0.01f, 0.1f);
        _audioFloat.pitch = Random.Range(0.8f, 1.1f);
        _audioFloat.Play();
    }


    void SpawnMessage(string message)
    {
        var newMessage = Instantiate(_text.gameObject, _text.gameObject.transform.position, _text.gameObject.transform.rotation, _messages.transform);
        newMessage.GetComponent<TMP_Text>().text = message;
    }

    // 
}
