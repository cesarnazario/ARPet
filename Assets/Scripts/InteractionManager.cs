using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class InteractionManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _spawnablePrefab;
    [SerializeField]
    private GameObject _pizzaPrefab;
    [SerializeField]
    private ARRaycastManager _arRaycastManager;
    [SerializeField]
    private Camera _arCamera;
    [SerializeField]
    private NavigationBaker _baker;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private TMP_Text _text;
    private ARPlaneManager _planeManager;
    // [SerializeField]
    // private LayerMask _petLayer;
    // [SerializeField]
    // private LayerMask _spawnableLayer;
    // private const string _tag = "Spawnable";

    private ARPet _pet = null;
    private List<ARRaycastHit> _hits = new List<ARRaycastHit>();
    private GameObject _petPrefab;
    private VerticalLayoutGroup _messages;
    public static bool IsPizzaActive = false;

    private void Awake()
    {
        _messages = FindObjectOfType<VerticalLayoutGroup>();
        _planeManager = GetComponent<ARPlaneManager>();
    }
    void Update()
    {
        if (Input.touchCount == 0)
        {
            return;
        }

        RaycastHit hit;
        var ray = _arCamera.ScreenPointToRay(Input.GetTouch(0).position);

        if (_arRaycastManager.Raycast(Input.GetTouch(0).position, _hits))
        {
            //Check state of touch
            switch (Input.GetTouch(0).phase)
            {
                case TouchPhase.Began:
                    if (_petPrefab == null)
                    {
                        if (Physics.Raycast(ray, out hit))
                        {
                            var Pet = hit.collider.GetComponent<ARPet>();
                            if (Pet != null)
                            {
                                if (_pet.IsAwake)
                                {
                                    _pet.Pet();
                                }
                                else
                                {
                                    _pet.WakeUp();
                                }
                                // _petPrefab = hit.collider.gameObject;
                                // _petPrefab.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
                            }
                            else
                            {
                                if (_pet == null)
                                {
                                    _baker.Rebake();
                                    _baker.DisablePlaneDetection();
                                    // SpawnMessage($"New pet in {hit.transform.position}");
                                    var PetClone = Instantiate(_spawnablePrefab, _hits[0].pose.position, Quaternion.identity);
                                    _pet = PetClone.GetComponent<ARPet>();
                                    return;
                                }
                            }
                            if (IsPizzaActive == false && _pet.IsAwake)
                            {
                                // _baker.Rebake();
                                // SpawnMessage($"Changing Destination to {hit.transform.position}");
                                var clonePizza = Instantiate(_pizzaPrefab, _hits[0].pose.position, Quaternion.identity);
                                _pet.SetNavMeshDestination(_hits[0].pose.position);
                                IsPizzaActive = true;
                            }
                        }
                    }
                    break;
                case TouchPhase.Moved:
                    if (_petPrefab != null)
                    {
                        // _petPrefab.transform.position = _hits[0].pose.position;
                        // _petPrefab.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                    }
                    break;
                case TouchPhase.Ended:
                    if (_petPrefab != null)
                    {
                        // _petPrefab.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                        // _petPrefab = null;
                    }
                    break;
            }
        }
    }
    void SpawnMessage(string message)
    {
        var newMessage = Instantiate(_text.gameObject, _text.gameObject.transform.position, _text.gameObject.transform.rotation, _messages.transform);
        newMessage.GetComponent<TMP_Text>().text = message;
    }
    public void Pet()
    {
        if (_pet != null)
        {
            _pet.Pet();
        }
    }
    public void WakeUp()
    {
        if (_pet != null)
        {
            _pet.WakeUp();
        }
    }
    public void PlayLullaby()
    {
        _audio.Play();
        _pet.Sleep();

    }
    public void ResetScene()
    {
        SceneManager.LoadScene("ARPet");
    }
}



