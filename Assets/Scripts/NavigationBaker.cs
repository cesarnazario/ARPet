using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.Samples;

public class NavigationBaker : MonoBehaviour
{
    [SerializeField]
    private List<NavMeshSurface> _surfaces = new List<NavMeshSurface>();
    [SerializeField]
    private TMP_Text _text;
    private ARPlaneManager _planeManager;
    private VerticalLayoutGroup _messages;

    void Awake()
    {
        _planeManager = GetComponent<ARPlaneManager>();
        _messages = FindObjectOfType<VerticalLayoutGroup>();
    }

    private void GetAllPlanes()
    {
        foreach (var plane in _planeManager.trackables)
        {
            var newPlane = plane.GetComponent<NavMeshSurface>();
            _surfaces.Add(newPlane);
        }
    }

    public void Rebake()
    {
        // SpawnMessage($"Baking {_surfaces.Count} surfaces into Navmesh");
        GetAllPlanes();
        for (int i = 0; i < _surfaces.Count; i++)
        {
            _surfaces[i].BuildNavMesh();

        }
    }


    public void DisablePlaneDetection()
    {
        _planeManager.enabled = false;
        foreach (var plane in _planeManager.trackables)
        {
            plane.GetComponent<MeshRenderer>().enabled = false;
            plane.GetComponent<ARPlaneMeshVisualizer>().enabled = false;
            plane.GetComponent<ARFeatheredPlaneMeshVisualizer>().enabled = false;
            //plane.gameObject.SetActive(value);
        }
    }

    void SpawnMessage(string message)
    {
        var newMessage = Instantiate(_text.gameObject, _text.gameObject.transform.position, _text.gameObject.transform.rotation, _messages.transform);
        newMessage.GetComponent<TMP_Text>().text = message;
    }
}



